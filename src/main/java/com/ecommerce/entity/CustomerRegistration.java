package com.ecommerce.entity;

import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class CustomerRegistration {
	private int customerId;
	private String customerName;
	private String customerEmail;
	private long customerphoneNumber;
	private String password;
}
